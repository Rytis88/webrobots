package com.scraping;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Movies {

	static BufferedWriter bufferedWriter = null;
	static FileOutputStream fileOutputStream = null;
	final static String FILE_HEADER = "Rank, Name, Year, Rating, Url, Director, No_of_reviews, Image, Description, Source_url";
	final static String NEW_LINE_SEPARATOR = "\n";
	final static String COMMA_DELIMITER = ",";
	static File file = new File("Movies.csv");

	public static void main(String[] args) {

		try {
			fileOutputStream = new FileOutputStream(file);
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
			final Document document = Jsoup.connect("https://www.imdb.com/chart/top").get();
			bufferedWriter.append(FILE_HEADER);
			bufferedWriter.append(NEW_LINE_SEPARATOR);

			for (org.jsoup.nodes.Element element : document.select("table.chart.full-width tbody tr")) {
				String rank = element.select("td span").attr("data-value");
				String name = element.select(".titleColumn a").text();
				String year = element.select(".titleColumn span").text();
				String rating = element.select(".ratingColumn.imdbRating").text();
				String url = element.select("td a").attr("href");
				url = "https://www.imdb.com" + url;
				String titleAttribute = element.select("td a").attr("title");
				String director = titleAttribute.substring(0, titleAttribute.indexOf("(dir.)"));
				String noOfReviews = element.select("td span:nth-child(4)").attr("data-value");
				String image = element.select("td a img").attr("src");
				String movieUrl = element.select("td a").attr("href");
				Document document1 = Jsoup.connect(url).get();
				String description = document1.select("#title-overview-widget .plot_summary_wrapper .summary_text").text();
				String Source_url = "https://www.imdb.com/chart/top";
				

				
				bufferedWriter.append(rank);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(name);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(year);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(rating);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(url);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(director);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(noOfReviews);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(image);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(movieUrl);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(description);
				bufferedWriter.append(COMMA_DELIMITER);
				bufferedWriter.append(Source_url);
				bufferedWriter.append(NEW_LINE_SEPARATOR);
				
				

			}
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
